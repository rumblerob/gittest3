﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace gitTest3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "this text is set in the controller, which is terrifying";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "about about about";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "contact contact contact";

            return View();
        }
    }
}
